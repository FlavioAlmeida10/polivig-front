import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login.component';

import {MatMenuModule} from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { LoginService } from 'src/app/service/login.service';

import { BoardAdminComponent } from './board-admin/board-admin.component';
import {ButtonModule} from 'primeng/button';

@NgModule({
  declarations: [
    LoginComponent,
    BoardAdminComponent

  ],
  imports: [
    CommonModule,
    FormsModule,
    CommonModule, 
    MatToolbarModule,
    MatButtonModule, 
    MatCardModule,
    MatInputModule,
    MatDialogModule,
    MatTableModule,
    MatMenuModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    ButtonModule
  ],
  exports:[LoginComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers:[LoginService]
})
export class LoginModule { }
