import { Attribute, Component, OnInit } from '@angular/core';

import { MessageService } from 'primeng/api';
import { AtividadePrincipal } from 'src/app/model/atividade.principal';
import { AtividadesSecundarias } from 'src/app/model/atividades.secundarias';
import { CEP } from 'src/app/model/cep.consulta';

import { Empresa } from 'src/app/model/empresa';
import { Estado } from 'src/app/model/estado';
import { QSA } from 'src/app/model/qsa';
import { EmpresaService } from 'src/app/service/empresa.service';
import { EstadoService } from 'src/app/service/estado.service';

@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.scss']
})
export class EmpresaComponent implements OnInit {

  dadosEmpresa: Empresa = new Empresa();
  atividadeprincipal: AtividadePrincipal[]=[];
  atividadessecundarias: AtividadesSecundarias[]=[];
  qsa: QSA[]=[];
  qsaPropietario: QSA = new QSA();
  cepQsa: CEP= new CEP();
  isValido: Boolean = false;
  isAtivo: Boolean = true;
  isValidoSociedade:Boolean = false;
  tituloSocio: string='';
  disabled: boolean = true;
  disabledProp: boolean = true;
  titleDoc: string = 'Dados Pessoais (LGPD)'
  valido: string = '';
  status: string ='';
  proprietario: boolean = false;
  estados: Estado[]=[];
  selectEstado?: Estado;
  checked: boolean= false;
  disabledDadosPessoais: boolean= true;
  mascara: boolean = true;

  constructor(private empresa: EmpresaService, private messageService: MessageService,private estadoService: EstadoService) { }

  ngOnInit(): void {
      this.estadoSearch();
  }
  submitSave(dados: Empresa) {
    this.dadosEmpresa.cnpj.replace(".","").replace(".","").replace("/","").replace("-","");
    this.dadosEmpresa.cep?.replace("-","");
    if(this.dadosEmpresa.qsa.length == 0){
      
      this.qsaPropietario.cpf?.replace(".","").replace(".","").replace("-","");
      this.qsaPropietario.rg?.replace(".","").replace(".","").replace("-",'');
      this.qsaPropietario.telefone?.replace("(","").replace(")","").replace(".","").replace("-","");
      console.log("Saida proback: " + JSON.stringify(this.qsaPropietario));
      this.qsa.push(this.qsaPropietario);
    }else{
      this.qsa.filter(x =>{
        x.cep.cep.replace("-","");
        x.cpf?.replace(".","").replace(".","").replace("-","");
        x.rg?.replace(".","").replace(".","").replace("-",'');
        x.telefone?.replace("(","").replace(")","").replace(".","").replace("-","");
      })
    }
    this.empresa.postDadosEmpresa(dados).subscribe(response=>{
      this.dadosEmpresa = response;
    });
  }
  submitCancel(){
    this.dadosEmpresa = new Empresa();
    this.isValido = false;
    this.proprietario = false;
    this.isValidoSociedade = false;
    this.tituloSocio = '';
    this.ngOnInit();
  }

  getDadosEmpresa(cnpj: String){
    this.empresa.getDadosEmpresa(cnpj).subscribe(data =>{
      this.dadosEmpresa = data;
      
      console.log("Dados da Empresa: " + JSON.stringify(this.dadosEmpresa));

      if(!this.dadosEmpresa.fantasia){
        this.dadosEmpresa.fantasia = this.dadosEmpresa.nome;
      }
      this.atividadeprincipal = this.dadosEmpresa.atividade_principal;
      this.atividadessecundarias = this.dadosEmpresa.atividades_secundarias;
      this.qsa = this.dadosEmpresa.qsa;
      
      this.qsa.filter(x =>{
        x.cep = this.cepQsa;
        if(x.pais_origem == null){
          x.pais_origem = "Brasil";
        }
      })
     
      this.isValidoSociedade = true;
      if(this.qsa.length != 0){
        this.tituloSocio = 'Dados Sócios';
        this.proprietario = false;
      }else{
        this.tituloSocio = 'Dados Proprietário';
        this.proprietario = true;
        
      }
      if(this.dadosEmpresa.cnpj != null){
        this.isValido = true;
        this.valido = 'CNPJ';
        this.status = 'Empresa encontrada!';
        this.showSuccess(this.valido, this.status);
      }else{
        this.valido = 'CNPJ';
        this.status = 'Empresa não encontrada!';
        this.isValido = false;
        this.showWarn(this.valido, this.status);
      } 
    });
  }

  getCep(cep: String, qsas: QSA){
    this.empresa.getCep(cep).subscribe(data =>{
      this.qsa.filter(x=>{
        if(x.nome == qsas.nome){
          x.cep = data;
         
        if(qsas.cep){
          this.valido = 'CEP';
          this.status = 'Endereço encontrado!';
          this.showSuccess(this.valido, this.status);
        }else{
          this.valido = 'CEP';
          this.status = 'Endereço não encontrado!';
          this.showWarn(this.valido, this.status);
         }
        }
      })
    })
  }

  getCepProprietario(cep: String){
    this.empresa.getCep(cep).subscribe(data =>{
        this.qsaPropietario.cep = data;
        if(this.qsaPropietario.cep){
          this.valido = 'CEP';
          this.status = 'Endereço encontrado!';
          this.showSuccess(this.valido, this.status);
        }else{
          this.valido = 'CEP';
          this.status = 'Endereço não encontrado!';
          this.showWarn(this.valido, this.status);
         }
    })
  }

  //Autorizar uso Dados lei LGPD
  autorizarLGPD(event: any): void{
    if(event.checked == true){
      this.disabledDadosPessoais = false;
    }else{
      this.disabledDadosPessoais = true;
    }
      
  }

  //Pegar estados do back
  estadoSearch(){
    return this.estadoService.getAll().subscribe(data=>{
      this.estados = data;
      console.log(this.estados);
    })
  }

  //Remove caracteres CEP e CNPJ
  cnpjChangeFn(): void{
    var cnpj = this.dadosEmpresa.cnpj.replace(".","").replace(".","").replace("/","").replace("-","");
    this.getDadosEmpresa(cnpj);
  }
  cepChangeFn(qsas: QSA): void {
    var cep= qsas.cep.cep.replace("-","");
    qsas.cep.cep = '';
    this.getCep(cep, qsas);  
  }

  cepProprietarioChangeFn(): void {
    this.qsaPropietario.cep.cep.replace("-","");
    var cep= this.qsaPropietario.cep.cep.replace("-","");
    this.getCepProprietario(cep);  
  }

  //Menssagens Tela
  showSuccess(valido: string, status: string): void {
    this.messageService.add({severity:'success', summary: valido, detail: status});
  }
  showWarn(valido: string, status: string) {
    this.messageService.add({severity:'error', summary: valido, detail: status});
  }
}
