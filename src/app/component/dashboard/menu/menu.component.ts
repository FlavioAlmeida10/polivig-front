import { Component, OnInit } from '@angular/core';
import { MenuItem, PrimeNGConfig } from 'primeng/api';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  items: MenuItem[]=[];
  constructor(private primengConfig: PrimeNGConfig) { }

  ngOnInit(): void {
    this.geradorMenu();
    this.primengConfig.ripple = true;
  }


  geradorMenu(){
    this.items = [
      {
        label: 'Empresa',
        icon: 'pi pi-building',
        items: [
          {label: 'Dados da Empresa', icon: 'pi pi-building', routerLink:"/empresa" },
          {label: 'Dados Pagamentos', icon: 'pi pi-fw pi-book', routerLink:'/empresa' },
          {label: 'Notas Fiscais', icon: 'pi pi-fw pi-book', routerLink:'/empresa' },
          {separator: true},
        ]
    },
    {
        label: 'Clientes',
        icon: 'pi pi-fw pi-users',
        items: [
          {label: 'Dados Cliente', icon: 'pi pi-building' },
          {label: 'Dados Pagamentos', icon: 'pi pi-fw pi-book'},
          {label: 'Notas Fiscais', icon: 'pi pi-fw pi-book'},
          {separator: true},
        ]
    },
    {
        label: 'Funcionários',
        icon: 'pi pi-fw pi-sitemap',
        items: [
          {label: 'Dados Funcionários', icon: 'pi pi-building' },
          {label: 'Dados Pagamentos', icon: 'pi pi-fw pi-book'},
          {label: 'Controle de Ponto', icon: 'pi pi-fw pi-book'},
          {label: 'Folha de Pagamento', icon: 'pi pi-fw pi-book'},
          {label: 'Folha de Pagamento', icon: 'pi pi-fw pi-book',
          items:[
            {label: 'Lançamentos', icon: 'pi pi-fw pi-book'},
            {label: 'Fechamento', icon: 'pi pi-fw pi-book'},
            {label: 'ETC', icon: 'pi pi-fw pi-book'}
          ]},
          {separator: true},
        ]
    },
    {
        label: 'Manutenções',
        icon: 'pi pi-fw pi-cog',
        items: [
            { label: 'Tabelas', icon: 'pi pi-fw pi-pencil' },
            { label: 'Autorizações', icon: 'pi pi-fw pi-tags'},
            { label: 'Usuários', icon: 'pi pi-fw pi-tags'},
            { label: 'Compras', icon: 'pi pi-fw pi-tags'},
            { label: 'Restauração', icon: 'pi pi-fw pi-tags'}
        ]
    }
    ];
  }
}
