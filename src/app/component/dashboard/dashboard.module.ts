import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer/footer.component';
import { MenuComponent } from './menu/menu.component';
import { DashboardComponent } from './dashboard.component';
import { HeaderComponent } from './header/header.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { ComponentModule } from '../component.module';
import { PanelMenuModule } from 'primeng/panelmenu';
import {TabMenuModule} from 'primeng/tabmenu';
import {MenuModule} from 'primeng/menu';
import { ContentComponent } from './content/content.component';

import { ButtonModule } from 'primeng/button';
import { EmpresaComponent } from './pages/empresa/empresa.component';
import { DashboardRoutingModule } from './dashboard-routing.module';



@NgModule({
  declarations: [
    DashboardComponent,
    MenuComponent,
    FooterComponent,
    HeaderComponent,
    ContentComponent,
    EmpresaComponent
  ],
  imports: [
    DashboardRoutingModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    ComponentModule,
    PanelMenuModule,
    TabMenuModule,
    MenuModule,
    ButtonModule
  ],
  exports:[
    DashboardComponent,
    MenuComponent,
    FooterComponent,
    HeaderComponent,
    ContentComponent,
    EmpresaComponent
  ],
  bootstrap: [HeaderComponent],
})
export class DashboardModule { }
