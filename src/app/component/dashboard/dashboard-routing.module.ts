import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { EmpresaComponent } from './pages/empresa/empresa.component';

const routes: Routes = [
    {path: '', component: DashboardComponent, children:[
      { path: 'empresa', component: EmpresaComponent }
    ]}
           
  ];
  
  @NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
  export class DashboardRoutingModule { }