import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { Estado } from '../model/estado';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class EstadoService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<Array<Estado>>(API_URL + "estados");
  }
}
