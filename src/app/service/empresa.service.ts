import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { Empresa } from '../model/empresa';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class EmpresaService {

  constructor(private http: HttpClient) { }

  getDadosEmpresa(cnpj: String): Observable<any> {
    return this.http.get(API_URL + 'empresa/' + cnpj);
  }

  getCep(cep: String): Observable<any> {
    return this.http.get(API_URL + 'correios/' + cep);
  }

  postDadosEmpresa(empresa: Empresa): Observable<any> {
    return this.http.post(API_URL + 'empresa', empresa);

  }
}
