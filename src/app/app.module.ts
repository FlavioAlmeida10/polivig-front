import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { LoginModule } from './component/login/login.module';
import { DashboardModule } from './component/dashboard/dashboard.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ComponentModule } from './component/component.module';
import {LayoutModule} from '@angular/cdk/layout';
import { CommonModule } from '@angular/common';
import { EmpresaService } from './service/empresa.service';
import { MessageService } from 'primeng/api';
import { EstadoService } from './service/estado.service';


@NgModule({
  declarations: [
    AppComponent,

  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    LoginModule,
    DashboardModule,
    HttpClientModule,
    ComponentModule,
    LayoutModule,
    DashboardModule
  ],
  exports:[
    FormsModule,
    
  ],
  providers: [
   EmpresaService,
   EstadoService,
   MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
