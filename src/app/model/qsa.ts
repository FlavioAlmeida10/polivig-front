
import { CEP } from "./cep.consulta";

export class QSA {
    public nome?: string;
    public qual?: string;
    public pais_origem?: string;
    public nome_rep_legal?: string;
    public qual_rep_legal?: string;
    public cep: CEP= new CEP();

    public autorizado?: boolean;
    public cpf?: string;
    public rg?: string;
    public orgExp?: string;
    public ufExp?: string;
    public dataExp?: string;
    public ctps?: string;
    public serie?: string;
    public ctpsUf?: string;
    public email?: string;
    public telefone?: string;


    constructor( nome?: string, qual?: string, pais_origem?: string, nome_rep_legal?: string, qual_rep_legal?: string, cep: CEP= new CEP(), autorizado?: boolean, cpf?: string, rg?: string, orgExp?: string, ufExp?: string,
    dataExp?: string, ctps?: string, serie?: string, ctpsUf?: string , email?: string, telefone?: string) {
        this.nome = nome;
        this.qual = qual;
        this.pais_origem = pais_origem;
        this.nome_rep_legal = nome_rep_legal;
        this.qual_rep_legal = qual_rep_legal;
        this.cep = cep;
        this.autorizado = autorizado;
        this.cpf= cpf;
        this.rg= rg;
        this.orgExp= orgExp;
        this.ufExp= ufExp;
        this.dataExp= dataExp;
        this.ctps= ctps;
        this.serie= serie;
        this.ctpsUf = ctpsUf;
        this.email= email
        this.telefone= telefone;
     
    }
}