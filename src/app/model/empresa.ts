import { AtividadePrincipal } from "./atividade.principal";
import { AtividadesSecundarias } from "./atividades.secundarias";
import { QSA } from "./qsa";

export class Empresa {

    public status?: string;
    public ultima_atualizacao?: string;
    public cnpj: string='';
    public tipo?: string;
    public porte?: string;
    public nome?: string;
    public fantasia?: string;
    public abertura?: string;
    public atividade_principal: AtividadePrincipal[]=[];
    public atividades_secundarias: AtividadesSecundarias[]=[];
    public natureza_juridica?:string;
    public logradouro?:string;
    public numero?: string;
    public complemento?: string;
    public cep?: string;
    public bairro?: string;
    public municipio?: string;
    public uf?: string;
    public email?: string;
    public telefone?: string;
    public efr?: string;
    public situacao?: string;
    public data_situacao?: string;
    public motivo_situacao?: string;
    public situacao_especial?: string;
    public data_situacao_especial?: string;
    public capital_social?: string;
    public qsa: QSA[]=[];


    constructor(status?: string, ultima_atualizacao?: string, cnpj: string='', tipo?: string, porte?: string, nome?: string, fantasia?: string, abertura?: string
        ,atividade_principal: AtividadePrincipal[]=[], atividades_secundarias: AtividadesSecundarias[]=[], natureza_juridica?: string, logradouro?: string, numero?: string, complemento?: string, cep?: string, bairro?: string
            ,municipio?: string, uf?: string, email?: string, telefone?: string, efr?: string, situacao?: string, data_situacao?: string, 
            motivo_situacao?: string, situacao_especial?: string, data_situacao_especial?: string, capital_social?: string, qsa: QSA[]=[]) {

        this.status = status;
        this.ultima_atualizacao = ultima_atualizacao;
        this.cnpj = cnpj;
        this.tipo = tipo;
        this.porte = porte;
        this.nome = nome;
        this.fantasia = fantasia;
        this.abertura = abertura;
        this.atividade_principal = atividade_principal;
        this.atividades_secundarias = atividades_secundarias;
        this.natureza_juridica = natureza_juridica;
        this.logradouro = logradouro;
        this.numero = numero;
        this.complemento = numero;
        this.cep = cep;
        this.bairro = bairro;
        this.municipio = municipio;
        this.uf = uf;
        this.email = email;
        this.telefone = telefone;
        this.efr = efr;
        this.situacao = situacao;
        this.data_situacao = data_situacao;
        this.motivo_situacao = motivo_situacao;
        this.situacao_especial = situacao_especial;
        this.data_situacao_especial = data_situacao_especial;
        this.capital_social = capital_social;
        this.qsa = qsa;
  
    }

}
