export class Estado {
    
    public id_uf?: number;
    public nome?: string;
    public sigla?: string;

    constructor(id_uf?: number, nome?: string, sigla?: string){
        this.id_uf = id_uf;
        this.nome = nome;
        this.sigla = sigla;
    }
}
